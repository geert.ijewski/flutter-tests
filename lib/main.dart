// Flutter code sample for

// This example shows a [BottomNavigationBar] as it is used within a [Scaffold]
// widget. The [BottomNavigationBar] has three [BottomNavigationBarItem]
// widgets and the [currentIndex] is set to index 0. The selected item is
// amber. The `_onItemTapped` function changes the selected item's index
// and displays a corresponding message in the center of the [Scaffold].
//
// ![A scaffold with a bottom navigation bar containing three bottom navigation
// bar items. The first one is selected.](https://flutter.github.io/assets-for-api-docs/assets/material/bottom_navigation_bar.png)

import 'package:flutter/material.dart';
import 'package:quiver/time.dart';

void main() => runApp(MyApp());

/// This Widget is the main application widget.
class MyApp extends StatelessWidget {
  static const String _title = 'Flutter Code Sample';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: _title,
      home: MyStatefulWidget(),
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  MyStatefulWidget({Key key}) : super(key: key);

  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

class BodyBusiness extends StatefulWidget {
  BodyBusiness({Key key}) : super(key: key);

  final state = _BodyBusinessState();

  @override
  _BodyBusinessState createState() {
    return state;
  }
  void incrementCounter() {
    state.incrementCounter();
  }
}

class _BodyBusinessState extends State<BodyBusiness> {
  final TimeOfDay currentTime = TimeOfDay.now();
  int clicked = 0;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
          children: <Column>[
            Column(children: <Widget>[
              Center(child: Text(
                "final time",
                style: TextStyle(fontSize: 30, color: Colors.greenAccent),
              ))]),
            Column(children: [Text(
              currentTime.toString(),
            )]),
            Column(children: [Text(
              clicked.toString()
            )]),
          ]
      ),
    );
  }

  void incrementCounter() {
    ++clicked;
  }
}

class BodyHome extends StatefulWidget {
  BodyHome({Key key}) : super(key: key);

  @override
  _BodyHomeState createState() => _BodyHomeState();
}

class _BodyHomeState extends State<BodyHome> {
  @override
  Widget build(BuildContext context) {
    TimeOfDay currentTime = TimeOfDay.now();
    return Container(
       child: Row(
         children: <Column>[
           Column(children: <Widget>[Text("varialbe time")]),
           Column(children: <Widget>[
              Center(child: Text(
                  "bbb",
                  style: TextStyle(fontSize: 30, color: Colors.greenAccent),
              ))]),
           Column(children: [Text(
               currentTime.toString(),
             )])
         ]
       ),
    );
  }
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  int _selectedIndex = 0;
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  var bodies = <Widget>[new BodyHome(), new BodyBusiness()];


  void _onItemTapped(int index) {
    setState(() {
      assert(index < bodies.length);
      if(index == 1) {
        (bodies[1] as BodyBusiness).incrementCounter();
      }
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('BottomNavigationBar Sample'),
      ),
      body: bodies.elementAt(_selectedIndex),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('Home'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.business),
            title: Text('Business'),
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.amber[800],
        onTap: _onItemTapped,
      ),
    );
  }
}
